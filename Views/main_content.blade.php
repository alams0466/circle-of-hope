<style>
	.tag-row {
		padding: 6px 10px;
		background: #f7f7f7;
		border: 1px solid #294c88;
		border-radius: 8px;
		color: #32439e;
	}

	.bg-change:hover {
		background-color: #32439e61;
		color: blue;
		border-color: blue;
	}
</style>
<div class="main_content">
	<div class="mcontainer">
		<!--  Feeds  -->
		<div class="lg:flex lg:space-x-10">
			<div class="lg:w-3/4  space-y-7">
					<div class="container-fluid">
					<?php if(isset($_GET['help-someone'])){ 
					 $disply = 'display:none;';
					?>
					
					 <div class="row">
							<div class="mb-1 overflow-hidden relative rounded-lg w-full">
								<img src="/img/Help-Someone2.png" alt="" class="w-full" style="height:auto;">
							</div>	
					  </div>
					<?php }else{ $disply = ''; ?>
					
					<div class="row">
						  
                            <?php if(!empty($quotes->image)){?>
							<div class="mb-1 overflow-hidden relative rounded-lg w-full">
								<img src="https://admin.circleofhope.co.in/media_library/{{$quotes->image}}" alt="" class="w-full" style="height:auto;">
							</div>	
							<?php } if(!empty($quotes->description)){?>
							
							<div class="p-4  rounded-lg" style="background-color: #f56b40;">
								<h1 class=" text-center text-white lg:text-2xl text-xl font-semibold mb-6">{{$quotes->description??''}}</h1>
							</div>
							<?php } ?>
						
					</div>
					<?php } ?>
				</div>
				<?php if (!is_null(Auth::user()->avatar)) {
					$image = 'https://admin.circleofhope.co.in/avatar/' . Auth::user()->avatar;
				} else {
					$image = '/img/user-thumb.png';
				} ?>
				<div style="{{$disply}}" class="card lg:mx-0 p-4" uk-toggle="target: #create-post-modal">
					<div class="flex space-x-3">
						<img src="{{$image}}" class="w-10 h-10 rounded-full">
						<input placeholder="Share your feelings..." class="bg-gray-100 hover:bg-gray-200 flex-1 h-10 px-6 rounded-full" autofocus>
					</div>
					<div class="grid grid-flow-col pt-3 -mx-1 -mb-1 font-semibold text-sm">
						<div class="hover:bg-gray-100 flex items-center p-1.5 rounded-md cursor-pointer">
							<svg class="bg-blue-100 h-9 mr-2 p-1.5 rounded-full text-blue-600 w-9 -my-0.5 hidden lg:block" data-tippy-placement="top" title="Tooltip" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
							</svg>
							Photo
						</div>
						<div class="hover:bg-gray-100 flex items-center p-1.5 rounded-md cursor-pointer">
							<svg class="bg-red-100 h-9 mr-2 p-1.5 rounded-full text-red-600 w-9 -my-0.5 hidden lg:block" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
							</svg>
							Feelings/Emotions
						</div>
						<div class="hover:bg-gray-100 flex items-center p-1.5 rounded-md cursor-pointer">
							<svg class="bg-green-100 h-9 mr-2 p-1.5 rounded-full text-orange-600 w-9 -my-0.5 hidden lg:block" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
								<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
							</svg>
							Category
						</div>

					</div>
				</div>
				<div id="post">
					
				</div>
				<div class="flex justify-center mt-6">
					<a href="javascript:;" class="bg-white dark:bg-gray-900 font-semibold my-3 px-6 py-2 rounded-full shadow-md dark:bg-gray-800 dark:text-white load-more-button">Load more ..</a>
				</div>
			</div>
				<div class="lg:w-1/4 w-full" > 
					<div uk-sticky="offset:100" class="uk-sticky">
						<h2 class="text-lg font-semibold mb-3"> Recently Posted </h2>
						<ul> 
						@foreach($recentPost as $recents)
						<?php 
							
							 $string = strip_tags($recents->content);
								if (strlen($string) > 30) {

									// truncate string
									$stringCut = substr($string, 0, 30);
									$endPoint = strrpos($stringCut, ' ');

									$string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
									$string .= '...';
								}
						    ?>
							<li>
								<a href="/post/{{$recents->id}}" class="hover:bg-gray-100 rounded-md p-2 -mx-2 block">
									<h3 class="font-medium line-clamp-2">{{$string}}</h3>
									<div class="flex items-center my-auto text-xs space-x-1.5">
									  <div>{{ date("M d Y, H:i a", strtotime($recents->created_at)) }}</div> <div class="pb-1"> </div>   
								   </div> 
								</a>
							</li>
                         @endforeach
					  
						</ul>
					<hr class="mt-1 mb-1">
					<div class="row">	 
					<div class="col-lg-12">	 
						<h4 class="text-lg font-semibold mt-2 mb-3"> Tags </h4>
						  <div class="flex flex-wrap gap-2">
							<?php
							 foreach($trending_tags as $key => $val){ 							 
							  if($key==7){  break;  }
							 ?>
							  <a href="/home?tag={{$val->name}}" class="tag-row bg-change py-1.5 px-4 rounded-full">{{ucfirst($val->name)}}</a>
							  <?php } ?>  
						  </div>
						  <div class="pt-3">
						   <a href="/all-tags" class="bg-blue-500 rounded-md text-white pt-2 pb-2 px-4"> View All</a>
						  </div>
                        </div>
					 </div>
				</div>
			</div>
		</div>
	</div>
</div>