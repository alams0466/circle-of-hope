<!DOCTYPE html>

<html lang="en">

<!-- Mirrored from demo.foxthemes.net/socialitev2.1/feed.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Aug 2021 21:02:45 GMT -->

<!--head blade-->
@include('head')
<!--head blade-->

<body>
    <div id="wrapper">
        <!-- Header -->
		@include('main_header')
        <!-- Header -->
        

        <!-- sidebar -->
		@include('sidebar')
        <!-- sidebar -->
         

        <!-- Main Contents -->
		@include('main_content')
        <!-- Main Contents -->   
    </div>



    <!-- open chat box -
	@include('chat_box')
    <!-- open chat box -->



    <!-- story-preview -->
	<!--@include('story_preview')-->
    <!-- story-preview -->

    
    <!-- Craete post modal -->
	@include('post_modal')
    <!-- Craete post modal -->
	
	<!--tour modal-->
	@include('tourModal')
	<!--tour modal-->


 
    <!--global scripts-->
    <!-- global scripts -->
    <div class="loading">
        <img src="{{ url('/img/loading.gif') }}" style="width:70px;">
    </div>
	@include('scripts')
	
    @if(session()->has('message'))
	 <script>
        
		toastr.success('{{ session()->get('message') }}');

	 </script> 
	 @endif
	 
	 <script>
	 
	 $(document).ready(function() {
		 
	  $('#modal-center').click(function(e) {
			 e.preventDefault();
		}); 
		
	   if({{Auth::user()->is_tour_read}} == 0){
			
			UIkit.modal('#modal-center').toggle();
			
		}  
		
	 });
	 

	 </script>
	 
	 
    @stack('custom_script')
</body>

<!-- Mirrored from demo.foxthemes.net/socialitev2.1/feed.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Aug 2021 21:03:44 GMT -->
</html>