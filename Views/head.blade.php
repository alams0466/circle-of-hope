<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, height=device-height"/>
    <meta name="google-site-verification" content="g2yC72NJrt9MC2CG5v6vzBc4N1uVmZkikBQTZN65Als" />
    <!-- Favicon -->
    <link  href="/img/fevicon.jpg" rel="icon" type="image/png">

    <!-- Basic Page Needs
        ================================================== -->
    <title>Circle of hope</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="title" content="Circle of Hope - Share your feelings, Unbottle emotions - live brighter" />
    <meta name="description" content="Circle of Hope, is a peer-to-peer support community for sharing the ups and downs of life in your safe space.">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
   
    <!-- icons
    ================================================== -->
    <link rel="stylesheet" href="/assets/css/icons.css">

    <!-- CSS 
    ================================================== --> 
    <link rel="stylesheet" href="/assets/css/uikit.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/tailwind.css"> 
    <link rel="stylesheet" href="/assets/css/toastr.min.css">	
    <link rel="stylesheet" href="/assets/css/sweetalert.css">
	
	 
	<link rel="preconnect" href="https://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet">
	<link rel="stylesheet" href="/assets/css/choices.min.css">
	<link rel="stylesheet" href="/assets/css/emojionearea.css">
	<link rel="stylesheet" href="/assets/css/custom.css">
 
    <style>
        input , .bootstrap-select.btn-group button{
            background-color: #f3f4f6  !important;
            /* height: 44px  !important; */
            box-shadow: none  !important; 
        }
    </style>
	
	<style>
   .choices__inner {
        border: none;
        border-radius: 10px;
        background-color: #f3f4f6;
        padding: 3px 4.5px 0px;
    }

    .choices__input.choices__input--cloned {
        width: 100%;
    }
	</style>

</head> 