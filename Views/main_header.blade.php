<?php 
$disp = '';
if(Request::segment(1) == 'post'){
	$disp = 'd-none';
}else if(Request::segment(1) == 'all-tags'){
	$disp = 'd-none';
}?>
<header>
		<div class="header_wrap">
			<div class="header_inner mcontainer">
				<div class="left_side">
					<span class="{{$disp}} slide_menu" uk-toggle="target: #wrapper ; cls: is-collapse is-active">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path d="M3 4h18v2H3V4zm0 7h12v2H3v-2zm0 7h18v2H3v-2z" fill="currentColor"></path></svg>
					</span>

					<div id="logo">
						<a href="/home"> 
							<img src="/img/logo1.png" alt="" style="height:60px;">
							<img src="/img/logo1.png" class="logo_mobile" alt="">
						</a>
					</div>
				</div>
				 
			  <!-- search icon for mobile -->
				<div class="header-search-icon" uk-toggle="target: #wrapper ; cls: show-searchbox"> </div>
				<div class="header_search"> 
					<form method="GET" action="{{url('home')}}">
					   <input type="text" id="query" name="query" class="form-control" placeholder="Search Circle Of Hope" autocomplete="off" value="<?= $_GET["query"] ?? '' ?>">
				       <a href="javascript:;" onclick="$(this).closest('form').submit()"><i class="uil-search-alt"></i></a>
					</form>
				</div>

				<div class="right_side">

					<div class="header_widgets">
                      
					  <!--notifications-->
						<a href="#" class="is_icon" uk-tooltip="title: Notifications">
							<svg fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10 2a6 6 0 00-6 6v3.586l-.707.707A1 1 0 004 14h12a1 1 0 00.707-1.707L16 11.586V8a6 6 0 00-6-6zM10 18a3 3 0 01-3-3h6a3 3 0 01-3 3z"></path></svg>
							<span id="notification-count">0</span>
						</a>
						<div uk-drop="mode: click" class="header_dropdown">
							 <div  class="dropdown_scrollbar" data-simplebar>
								 <div class="drop_headline">
									 <h4>Notifications </h4>
									 <div class="btn_action">
										<a href="#" data-tippy-placement="left" onclick="deleteNotify();" title="Delete All">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M17 6h5v2h-2v13a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8H2V6h5V3a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v3zm1 2H6v12h12V8zm-4.586 6l1.768 1.768-1.414 1.414L12 15.414l-1.768 1.768-1.414-1.414L10.586 14l-1.768-1.768 1.414-1.414L12 12.586l1.768-1.768 1.414 1.414L13.414 14zM9 4v2h6V4H9z" fill="rgba(38,90,138,1)"/></svg>
										</a>
										<a href="#" data-tippy-placement="left" onclick="allRead();" title="Mark as read all">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M2.243 6.854L11.49 1.31a1 1 0 0 1 1.029 0l9.238 5.545a.5.5 0 0 1 .243.429V20a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V7.283a.5.5 0 0 1 .243-.429zM4 8.133V19h16V8.132l-7.996-4.8L4 8.132zm8.06 5.565l5.296-4.463 1.288 1.53-6.57 5.537-6.71-5.53 1.272-1.544 5.424 4.47z" fill="rgba(47,89,186,1)"/></svg>
										</a>
									</div>
								 </div>
								 <ul id="notification-list"> </ul> 
							 </div>
						</div> 

						<!-- Message -->
						<a href="#" onclick="allChatRead();" class="is_icon" uk-tooltip="title: Message">
							<svg fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z" clip-rule="evenodd"></path></svg>
							<span id="chat-notification-count">0</span>
						</a>
						<div uk-drop="mode: click" class="header_dropdown is_message">
							<div  class="dropdown_scrollbar" data-simplebar>
								<div class="drop_headline">
									 <h4>Messages </h4>
									<div class="btn_action">
										<a href="#" data-tippy-placement="left" title="Notifications">
											<ion-icon name="settings-outline" uk-tooltip="title: Message settings ; pos: left"></ion-icon>
										</a>
										<a href="#" data-tippy-placement="left" title="Mark as read all">
											<ion-icon name="checkbox-outline"></ion-icon>
										</a>
									</div>
								</div>
							
								<ul id="chat-notification">
									
								</ul>
							</div>
							<!--<a href="#" class="see-all"> See all in Messages</a>-->
						</div>
						
						<?php if(!is_null(Auth::user()->avatar)){
						$image = 'https://admin.circleofhope.co.in/avatar/'.Auth::user()->avatar;
						}else{
							$image = '/img/user-thumb.png';
						}?>
		
						<a href="#">
							<img src="{{$image}}" class="is_avatar" alt="">
						</a>
						<div uk-drop="mode: click;offset:5" class="header_dropdown profile_dropdown">

							<a href="#" class="user">
								<div class="user_avatar">
									<img src="{{$image}}" style="width:50px;height:45px;" alt="">
								</div>
								<div class="user_name">
									<div>{{ucfirst(Auth::user()->fname.' '.Auth::user()->lname)}}</div>
									<span> {{'@'.Auth::user()->fname}}</span>
								</div>
							</a>
							<hr>
							<a href="/my-profile?id={{Auth::user()->id}}">
								<svg fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"></path></svg>
								My Profile
							</a>
							<!--<a href="#" id="night-mode" class="btn-night-mode">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
									<path d="M17.293 13.293A8 8 0 016.707 2.707a8.001 8.001 0 1010.586 10.586z" />
								  </svg>
								 Night mode
								<span class="btn-night-mode-switch">
									<span class="uk-switch-button"></span>
								</span>
							</a>-->
							<a href="/logout">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
									<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"></path>
								</svg>
								Log Out 
							</a>

							
						</div>

					</div>
					
				</div>
			</div>
		</div>
	</header>

	