<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\AuthService;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Http\Requests\TagResponderRequest;
use App\Http\Requests\TestimonialRequest;
use App\Http\Requests\QuotesRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\RoleRequest;
use App\Services\QuotesService;
use App\Services\UserService;
use DB;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 *@return void
	 */
	private $quoteService = null;
	public function __construct()
	{
		
		$this->quotesService = new QuotesService();
		$this->userService = new UserService();
	}
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function home()
	{
		$curdate = date('Y-m-d');
		
		$quotes = DB::table('quotes')->where('post_date', 	$curdate)->where('post_status', 'posted')->first();
		if(empty($quotes)){
			$quotes = DB::table('media_library')->inRandomOrder()->first();
		}
		
		$data = array(
			'title' => 'Dashboard',
			'trending_tags' => DB::SELECT("select * from tags t, post_tags p where p.tag_id = t.id GROUP BY p.tag_id HAVING COUNT(p.tag_id) > 1 ORDER BY t.id DESC"),
			'tagss' => DB::SELECT("select * from tags order by id desc"),
			'category' => DB::table('feelings')->orderBy('id', 'desc')->get(),
			'quotes' => $quotes,
			'recentPost' => DB::table('posts')->whereNull('status')->orderBy('id', 'desc')->limit(3)->get(),
		);
		
    
		return view('index', $data);
	}
	public function getTagCategory()
	{
		$data['title'] =  'Tag category';
		$data['tagCategory'] = DB::Select("select * from tag_category order by id desc");
		return view('tag_category', $data);
	}
	public function getTags()
	{
		$data['title'] =  'Tags';
		$data['tags'] = DB::Select("select * from tags order by id desc");
		return view('tags_list', $data);
	}
	public function tagDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['tags_id'];
		DB::table('tags')->delete($id);
		return response()->json('TAG_DELETED');
	}
	public function editTag(Request $request)
	{
		$data = $request->all();
		$id = $data['tag_id'];
		$result = DB::Select("select * from tags where id = $id ");
		return response()->json($result[0]);
	}
	public function getTestimonial()
	{
		$data['title'] =  'Testimonial';
		$data['testimonial'] = DB::table('testimonials')->orderBy('id', 'DESC')->get();
		return view('testimonial_list', $data);
	}
	public function editTestimonial(Request $request)
	{
		$data = $request->all();
		$result = DB::table('testimonials')->where('id', $data['testimonial_id'])->first();
		return response()->json($result);
	}
	public function testimonialSubmitUpdate(TestimonialRequest $request)
	{
		$data = $request->all();
		$ArryData = array(
			'title' => $data['title'],
			'description' => $data['description']
		);
		if (isset($data['testimonial_id'])) {
			DB::table('testimonials')->where('id', $data['testimonial_id'])->update($ArryData);
			return response()->json('TESTIMONIAL_UPDATED');
		} else {
			DB::table('testimonials')->insert($ArryData);
			return response()->json('TESTIMONIAL_ADDED');
		}
	}
	public function testimonialDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['testimonialId'];
		DB::table('testimonials')->delete($id);
		return response()->json('TESTIMONIAL_DELETED');
	}
	public function getQuotes()
	{
		$data['title'] =  'Quotes list';
		$data['quotes'] = DB::table('quotes')->orderBy('id', 'DESC')->get();
		return view('quotes_list', $data);
	}
	public function manageQuotes($id = null)
	{
		$data['title'] =  'Create Quotes';
		if ($id) {
			$query = DB::table('quotes')->where('id', $id)->first();
			$data['title'] = 'Manage Quotes';
			$data['id'] = $query->id;
			$data['author_name'] = $query->author_name;
			$data['description'] = $query->description;
			$data['image'] = $query->image;
		}
		return view('manage_quotes', $data);
	}
	public function quotesAddUpdate(QuotesRequest $request)
	{
		$user = AuthService::getUserModel();
		$data = $request->all();
		$files = $request->file('image');
		$result = $this->quotesService->quotesAddUpdate($data, $files, $user);
		return redirect('quotes-list')->with('message', 'Success !');
	}
	public function quotesDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['quotesId'];
		DB::table('quotes')->delete($id);
		return response()->json('QUOTES_DELETED');
	}
	public function getRoles()
	{
		$data['title'] =  'Roles list';
		$data['roles'] = DB::table('roles')->orderBy('id', 'DESC')->get();
		return view('roles_list', $data);
	}
	public function roleAddUpdate(RoleRequest $request)
	{
		$data = $request->all();
		$ArryData = array(
			'name' => $data['role']
		);
		if (isset($data['role_id'])) {
			DB::table('roles')->where('id', $data['role_id'])->update($ArryData);
			return response()->json('ROLE_UPDATED');
		} else {
			DB::table('roles')->insert($ArryData);
			return response()->json('ROLE_ADDED');
		}
	}
	public function categoryAddUpdate(CategoryRequest $request)
	{
		$data = $request->all();
		$ArryData = array(
			'name' => $data['tagCategory']
		);
		if (isset($data['tagCategoryId'])) {
			DB::table('tag_category')->where('id', $data['tagCategoryId'])->update($ArryData);
			return response()->json('CATEGORY_UPDATED');
		} else {
			DB::table('tag_category')->insert($ArryData);
			return response()->json('CATEGORY_ADDED');
		}
	}
	public function categoryEdit(Request $request)
	{
		$data = $request->all();
		$result = DB::table('tag_category')->where('id', $data['CatId'])->first();
		return response()->json($result);
	}
	public function categoryDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['CatId'];
		DB::table('tag_category')->delete($id);
		return response()->json('CATEGORY_DELETED');
	}
	public function editRole(Request $request)
	{
		$data = $request->all();
		$result = DB::table('roles')->where('id', $data['role_id'])->first();
		return response()->json($result);
	}
	public function roleDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['roleId'];
		DB::table('roles')->delete($id);
		return response()->json('ROLE_DELETED');
	}
	public function getAutoResponderTags()
	{
		$data['title'] =  'Tag Message List';
		$data['tag_message'] = DB::Select("select * from tags t, tags_responder ts where ts.tag_id = t.id order by ts.id desc");
		return view('auto_respond_tag', $data);
	}
	public function manageTagResponder($id = null)
	{
		$data['title'] =  'Create Tag Message ';
		$data['tag_name'] = DB::Select("select * from tags order by id desc");
		if ($id) {
			$query = DB::table('tags_responder')->where('id', $id)->first();
			$data['title'] = 'Manage Tag Message';
			$data['id'] = $query->id;
			$data['tagId'] = $query->tag_id;
			$data['Tagmessage'] = $query->message;
		}
		return view('manage_tag_responder', $data);
	}
	public function manageTag($id = null)
	{
		$data['title'] =  'Create Tag ';
		$data['TagCategory'] = DB::Select("select * from tag_category order by id desc");
		if ($id) {
			$query = DB::table('tags')->where('id', $id)->first();
			$data['title'] = 'Manage Tag';
			$data['id'] = $query->id;
			$data['tag'] = $query->name;
			$data['tag_category_id'] = $query->tag_category_id;
			$data['used_tag'] = $query->used_tag;
		}
		return view('manage_tag', $data);
	}
	public function AddUpdateResponder(TagResponderRequest $request)
	{
		$data = $request->all();
		$result = $this->userService->AddUpdateResponder($data);
		return redirect('tag-message-list')->with('message', 'Success !');
	}
	public function tagResponderDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['tag_responder_id'];
		DB::table('tags_responder')->delete($id);
		return response()->json('TAG_RESPONDER_DELETED');
	}
	public function tagsAddUpdate(Request $request)
	{
		$data = $request->all();
		$result = $this->userService->tagAddUpdate($data);
		return redirect('tag-list')->with('message', 'Success !');
	}
	public function tagrowDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['id'];
		$condition = array('id' => $id);
		$delete_status = DB::table('tags')->delete($id);
		if ($delete_status == true) {
			echo "true";
		} else {
			echo "false";
		}
	}
	public function mediaLibrary()
	{
		$data['title'] = 'Media Library';
		$data['MediaLibrary'] = DB::table('media_library')->orderBy('id', 'DESC')->get();
		return view('media_library', $data);
	}
	public function AddMedia(Request $request)
	{
		$user = AuthService::getUserModel();
		$data = $request->file('image');
		foreach ($data as $file) {
			$directory_url =  public_path() . \AppConfig::MEDIA_DIR;
			$name = $user->id . '_' . time() . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
			$file->move($directory_url, $name);
			$img_arr = array('image' => $name);
			DB::table('media_library')->insert($img_arr);
		}
		return redirect('media-library')->with('message', 'Success !');
	}
	public function mediaDelete(Request $request)
	{
		$data = $request->all();
		$id = $data['media_id'];
		DB::table('media_library')->delete($id);
		return response()->json('MEDIA_DELETED');
	}
	
	public function getAllTags(){
		
		$tags = DB::table('tags')->orderBy('id', 'DESC')->get()->toArray();
		
		 $A = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'A'){	
				$A[] = $row->name;	
			   }										 
			}		

		$B = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'B'){	
				$B[] = $row->name;	
			   }										 
			}
			
		$C = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'C'){	
				$C[] = $row->name;	
			   }										 
			} 	
			
		$D = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'D'){	
				$D[] = $row->name;	
			   }										 
			} 
			
		$E = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'E'){	
				$E[] = $row->name;	
			   }										 
			} 	
			
		$F = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'F'){	
				$F[] = $row->name;	
			   }										 
			} 
			
		$G = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'G'){	
				$G[] = $row->name;	
			   }										 
			} 
			
		$H = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'H'){	
				$H[] = $row->name;	
			   }										 
			}
			
		$I = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'I'){	
				$I[] = $row->name;	
			   }										 
			} 
			
		$J = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'J'){	
				$J[] = $row->name;	
			   }										 
			}
			
		$K = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'K'){	
				$K[] = $row->name;	
			   }										 
			} 	
			
		$L = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'L'){	
				$L[] = $row->name;	
			   }										 
			} 	
			
		$M = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'M'){	
				$M[] = $row->name;	
			   }										 
			}	
			
		$N = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'N'){	
				$N[] = $row->name;	
			   }										 
			}
			
		$O = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'O'){	
				$O[] = $row->name;	
			   }										 
			} 		
			
		$P = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'P'){	
				$P[] = $row->name;	
			   }										 
			}
			
		$Q = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'Q'){	
				$Q[] = $row->name;	
			   }										 
			}	
			
		$R = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'R'){	
				$R[] = $row->name;	
			   }										 
			} 	
			
		$S = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'S'){	
				$S[] = $row->name;	
			   }										 
			}
			
		$T = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'T'){	
				$T[] = $row->name;	
			   }										 
			} 
			
		$U = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'U'){	
				$U[] = $row->name;	
			   }										 
			}	
			
		$V = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'V'){	
				$V[] = $row->name;	
			   }										 
			}
			
		$W = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'W'){	
				$W[] = $row->name;	
			   }										 
			}
 			
		$X = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'X'){	
				$X[] = $row->name;	
			   }										 
			}  	
			
		$Y = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'Y'){	
				$Y[] = $row->name;	
			   }										 
			} 
			
		$Z = [];
		 foreach($tags as $row){
			if(ucfirst(substr($row->name, 0, 1)) == 'Z'){	
				$Z[] = $row->name;	
			   }										 
			}  
		
		$tagsGroup = [
		     'A' => $A,
		     'B' => $B,
		     'C' => $C,
		     'D' => $D,
		     'E' => $E,
		     'F' => $F,
		     'G' => $G,
		     'H' => $H,
		     'I' => $I,
		     'J' => $J,
		     'K' => $K,
		     'L' => $L,
		     'M' => $M,
		     'N' => $N,
		     'O' => $O,
		     'P' => $P,
		     'Q' => $Q,
		     'R' => $R,
		     'S' => $S,
		     'T' => $T,
		     'U' => $U,
		     'V' => $V,
		     'W' => $W,
		     'X' => $X,
		     'Y' => $Y,
		     'Z' => $Z
		  ];						 
		
		
		$data = [ 
			'title' => 'Tags',
			'tags' => $tagsGroup
		   ];

		return view('allTags', $data);
		
	}
	
	
	public function getHelpLine(Request $request){
		
		$countryName = $request->get('countryName');
		
		$data = DB::table('resources_help_line')->get()->toArray();
		
		if(!empty($countryName)){
			$data = DB::table('resources_help_line')->where('countryName' ,$countryName)->get()->toArray();	
		}
		
		$HelpLine = [ 'HelpLine' => $data ];
		
		return view('resources', $HelpLine);
		
	}
}
