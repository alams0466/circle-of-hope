<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\SmsService;
use App\Services\UserLoginService;
use App\Services\UtilityService;
use App\Services\AuthService;
use Auth;
use DB;
use Redirect;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * @resource User
 *
 */
class UserController extends Controller
{
    private $userService = null;
    private $userLoginService = null;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->smsService = new SmsService();
        $this->userLoginService = new UserLoginService();
    }

	public function signup()
    {				   		
		 
	   $data['countries'] = DB::table('country')->get();

	   return view('signup',$data);

    }	
	
    /**
     * Login
     *
     * logs the user into the system
     */
    public function login(UserLoginRequest $request)
    {       
		
		$data = $request->all();
		
		if(empty($data['username'])){
		   
		  return Redirect::back()->withErrors([ 'message' => ' Please Input username or Email.' ]); 
	    }
		
		$loginUser = $data['username'];

		$email_id = $login_id = '';
		
		if(preg_match('/@.+\./', $loginUser)){
			
		 $email_id = $data['username'];
		} else 
		{ 
		  $login_id = $data['username'];
		}
		

	
		$result = $this->userService->loginUser($login_id, $email_id,$data['password']);


	   
	   if (isset($result['error']) && $result['error'] == 'ACCOUNT_NOT_FOUND') {
            
		 return Redirect::back()->withErrors([ 'message' => 'We could not find an account for this username. Please check your username or create a new account.' ]);
		
		}	   
		
		if (isset($result['error']) && $result['error'] == 'ACCOUNT_NOT_ACTIVE') {
            
		 return Redirect::back()->withErrors([ 'message' => ' Invalid Credentials.' ]);
		
		} 	
		
		if (isset($result['error']) && $result['error'] == 'ACCOUNT_BLOCKED') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Your account is Blocked please contact to Circle Of Hope Team.' ]);
		
		} 	
		
		if (isset($result['error']) && $result['error'] == 'ACCOUNT_INACTIVE') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Your account is Inactive please contact to Circle Of Hope Team.' ]);
		
		} 
		
	   if (isset($result['error']) && $result['error'] == 'LOGIN_INVALID_CREDENTIALS') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Invalid Credentials!' ]);
		
		}
		
	   if (isset($result['error']) && $result['error'] == 'EMAIL_NOT_VERIFIED') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Your email is not verified. Contact to Circle Of Hope Team at circleofhopeinfo@gmail.com.' ]);
		
		}


		$user  =  $result['user'];		
		Auth::login($user);
		
		//insert user logins..
        $values = [
					'user_id' => $user->id,
					'ip' => $_SERVER['REMOTE_ADDR'],
					'user_agent' => $request->header('User-Agent'),
					'locale' => app()->getLocale(),
					'city' => null,
					'region' =>  null,
					'country' =>  null,
					'latitude' => isset($data['latitude']) ? $data['latitude'] : null,
					'longitude' => isset($data['longitude']) ? $data['longitude'] : null,
					'postal' =>  null,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
		          ];
				  
		
		 DB::table('user_logins')->insert($values);	

        return redirect('/home');
    }
	
	
	    public function logout(Request $request)
    {
		$user = AuthService::getUserModel();
       
         if ($user) {
            $this->userLoginService->deleteByCondition(['user_id' => $user->id]);
        }

        Auth::logout();
        return redirect('/login');
    }
	
	public function geUsers(){
		
		 $data['title'] =  'User list';	
		 
		 $data['users'] = $this->userService->getUsers();
		return view('user_list', $data);
		 
	}
 

	public function userAdd(UserRequest $request)
    {	
		  
		  $data = $request->all();
		  
			if ($data["password"] == "" ||  $data["confirm_password"] == "") {
				
			 return Redirect::back()->withErrors([ 'message' => ' Empty Password.' ]);
			
			}
			
			
			if ($data["password"] != $data["confirm_password"]) {
				
			 return Redirect::back()->withErrors([ 'message' => ' Password Do not matched.' ]);
			
			} 
		  
		  $userid = $this->userService->userAdd($data);
		   
		  $trackingID = UtilityService::getEncrytedValue($userid);

		  return redirect('get-otp-verification/'.$trackingID);

    } 

    public function getOTPVerificationForm($tracking_id)
    {
             
		$userId = UtilityService::getDecryptedValue($tracking_id);
				
		$otp = UtilityService::generateUniqueId('users', 'mobile_otp', 4);
        
		$mobile_otp = array('mobile_otp' => $otp);
		
		$this->userService->insertOTP($mobile_otp, $userId);
		
		$data = DB::table('users')->where('id', $userId)->first();
		
        $result = $this->userService->sendOtpforSignup($data->email);
        
		$userData[ 'email'] = $data->email;
        
        return view('otp_verify_signup', $userData);
    }	
	
	public function deleteUser(Request $request)
    {				   		
		$data = $request->all();
		 
		$result = $this->userService->deleteUser($data['userId']);

	     return response()->json('USER_DELETED');	

    }	
	
	public function checkAlreadyExist(Request $request)
    {				   		
		$data = $request->all();
		
	
		$result = $this->userService->checkAlreadyExist($data['emailID']);  
		
		$tracking_id = UtilityService::getEncrytedValue($result['id']);
      
		if(!is_null($result)){
          
		  return response()->json($tracking_id);
		
		}else{
			
			 return response()->json('NOT_EXIST');	
		} 
    }	
	
	public function checkEmailyExist(Request $request)
    {				   		
		$data = $request->all();
		
	
		$result = $this->userService->checkAlreadyExist($data['emailID']);  		
      
		if(!is_null($result)){
          
		  return response()->json('ALREADY_EXIST');
		
		}else{
			
			 return response()->json('NOT_EXIST');	
		} 
    }

    //load OTP form
    public function ShowOtpForm($tracking_id)
    {
             
		$userId = UtilityService::getDecryptedValue($tracking_id);
				
		$otp = UtilityService::generateUniqueId('users', 'mobile_otp', 4);
        
		$mobile_otp = array('mobile_otp' => $otp);
		
		$this->userService->insertOTP($mobile_otp, $userId);
		
		$data = DB::table('users')->where('id', $userId)->first();
		
        $result = $this->userService->sendOtp($data->email);
        
		$userData[ 'email'] = $data->email;
        
        return view('otp_verify', $userData);
    }

    /**
     * Resend Otp
     *
     * resend otp
     */
    public function resendOtp(Request $request)
    {
        $data = $request->all();
        $result = $this->userService->resendOtp($data);
		
		if (isset($result['error'])) {
            
		 return response()->json('USER_NOT_FOUND');
		
		}

        $response['tracking_id'] = $result['tracking_id'];
        return response()->json('OTP_RESEND_SUCCESSFULLY');
    }	
	
	public function checkAlreadyExistUsername(Request $request)
    {				   		
		$data = $request->all();
		
		if(empty($data['userID'])){
			
		$result = $this->userService->checkAlreadyExistUsername($data['username']);
        
		if(count($result) > 0){
		  
		  return response()->json('ALREADY_EXIST');	
		
		}else{
			
			 return response()->json('NOT_EXIST');	
		} 
		
		}else{
			
		   return response()->json('NOT_EXIST');	
		}
		
    	

    }	
	
	    /**
     * Verify OTP Signup
     *
     * Verify the users otp code
     */
    public function verifyOTPSignup(Request $request)
    {
       
        $otp_code = $request->first . $request->second . $request->third . $request->fourth;
       
        $data = [
            'email' => $request->email,
            'otp_code' => $otp_code
        ];


       $result = $this->userService->verifyOTP($data);
      
	   if (isset($result['error']) && $result['error'] == 'INVALID_OTP') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Invalid OTP!' ]);
		
		}  	   
		
		if (isset($result['error']) && $result['error'] == 'USER_NOT_FOUND') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Invalid Account!' ]);
		
		}       
		
		
	   if (isset($result['error']) && $result['error'] == 'SERVER_ERROR') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Something went Wrong!' ]);
		
		}       
         
		 
		 
		 
        $arrayData = array(
		         'email_verified' => 1,
		         'mobile_otp' => null
		       );

		$retuen = $this->userService->email_verified($arrayData, $result['user']['id']);
		
		$track_id = UtilityService::getEncrytedValue($result['user']['id']);		 

		$user  =  $result['user'];	
		
		Auth::login($user);
		
		//insert user logins..
        $values = [
					'user_id' => $user->id,
					'ip' => $_SERVER['REMOTE_ADDR'],
					'user_agent' => $request->header('User-Agent'),
					'locale' => app()->getLocale(),
					'city' => null,
					'region' =>  null,
					'country' =>  null,
					'latitude' => isset($request->latitude) ? $request->latitude : null,
					'longitude' => isset($request->longitude) ? $request->longitude : null,
					'postal' =>  null,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
		          ];
				  
		 DB::table('user_logins')->insert($values);	
		 
		 $WelcomeNoti = [
		                  'from_user_id' => 2,
						  'user_id' => $user->id,
		                  'users_name' => $user->fname.' '.$user->lname,
		                  'notify' => 'Welcome to Circle Of Hope',
						  'is_system_notification' => 1,
						  'created_at' => date('Y-m-d H:i:s'),
					      'updated_at' => date('Y-m-d H:i:s')
							 
					   ];
		 
		 DB::table('notifications')->insert($WelcomeNoti);
		 
		 $this->smsService->sendGreetingMail($result);
		 
        return redirect('home')->with('message', 'Succefully Registered');
    }
	

    /**
     * Verify OTP
     *
     * Verify the users otp code
     */
    public function verifyOTP(Request $request)
    {

        $otp_code = $request->first . $request->second . $request->third . $request->fourth;
       
        $data = [
            'email' => $request->email,
            'otp_code' => $otp_code
        ];


       $result = $this->userService->verifyOTP($data);
      
	   if (isset($result['error']) && $result['error'] == 'INVALID_OTP') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Invalid OTP!' ]);
		
		}  	   
		
		if (isset($result['error']) && $result['error'] == 'USER_NOT_FOUND') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Invalid Account!' ]);
		
		}       
		
		
	   if (isset($result['error']) && $result['error'] == 'SERVER_ERROR') {
            
		 return Redirect::back()->withErrors([ 'message' => 'Something went Wrong!' ]);
		
		}       
         
        $arrayData = array(
		         'email_verified' => 1,
		         'mobile_otp' => null
		       );

		$this->userService->email_verified($arrayData, $result['user']['id']);
		
		 $track_id = UtilityService::getEncrytedValue($result['user']['id']);

        return redirect('set-new-password/'.$track_id);
    }
	
	public function newPasswordForm($track_id){
		
		$data['track_id'] = $track_id;
		
		return view('reset_password', $data);
	}
	
	
    /**
     * Reset Password
     *
     * reset password
     */
    public function resetPassword(Request $request)
    {
        $data = $request->all();
		
		
	if ($data["new_password"] == "" ||  $data["confirm_password"] == "") {
            
		 return Redirect::back()->withErrors([ 'message' => ' Empty Password.' ]);
		
		}	
		
	if ($data["new_password"] != $data["confirm_password"]) {
            
		 return Redirect::back()->withErrors([ 'message' => ' Password Do not matched.' ]);
		
		} 
		
		
        $result = $this->userService->resetPassword($data);
		
				
        if (isset($result['error']) && $result['error'] = 'USER_NOT_FOUND') {
			
            return Redirect::back()->withErrors([ 'message' => ' Invalid Account' ]);
        }
				

        return redirect('/login')->with('message', 'Success !');
    }
	
		
		public function registerEmail(Request $request){
			
          $data = $request->all();
         
		  $values = [
			'email' => $data['email'],
			'role_id' => 4
		  ];
				  
		   $userID = DB::table('users')->insertGetId($values);
		  
		   $user['track_id'] = UtilityService::getEncrytedValue($userID);
		   
		   return response()->json($user);	
		  
		}
		
		public function signupStepTwo($trackID){
			
			$data['userid'] = UtilityService::getDecryptedValue($trackID);
			
			return view('signupStepTwo', $data);
		}
		
		public function getNotification(){
			
           $user = AuthService::getUserModel();
		   
		   $notification = DB::table('notifications')->where('user_id', $user->id)->get();
		   
           $Array = [];
			
			foreach($notification as $val){
				
			$toUserData = DB::table('users')->where('id', $val->from_user_id )->first();
			
				
			$Array['notifications'][] = [
				    'id' => $val->id,
					'postId' => $val->post_id,
				    'from_user' => $toUserData->fname.' '.$toUserData->lname,
				    'image' => ($toUserData->avatar) ? \AppConfig::USER_RESOURCE_URL . $toUserData->avatar : '',
				    'notify' => $val->notify,
				    'is_read' => $val->is_read,
				    'is_system_notification' => $val->is_system_notification,
				    'created_at' => date("M d Y, H:i a", strtotime($val->created_at)),
				];
			}			
		  
		  return response()->json($Array);	
		}		
		
		public function countUnreadNotification(){
			
           $user = AuthService::getUserModel();
		   
		   $count = DB::table('notifications')->where('user_id', $user->id)->where('is_read', 0)->get();
		   
            $CountArray = [];
		   
            $CountArray['count'] = count($count);		   

		  return response()->json($CountArray);	
		}

		
		public function readNotification(Request $request){
		   $data = $request->all();
		   
		   $arrUpdate = [ 'is_read' => 1 ];
		   
		   DB::table('notifications')->where('id', $data['notification_id'])->update($arrUpdate);
		  return response()->json("READ_SUCCESS");	
		}

		
		public function allReadNotification(){

		    $user = AuthService::getUserModel();
			
		    $arrUpdate = [ 'is_read' => 1 ];
		   
		   DB::table('notifications')->where('user_id', $user->id)->update($arrUpdate);
		  return response()->json("READ_ALL_SUCCESS");	
		}
			
		public function deleteNotification(){

		   $user = AuthService::getUserModel();			
		   
		   DB::table('notifications')->where('user_id', $user->id)->delete();
		  return response()->json("DELETE_ALL_SUCCESS");	
		}


			
	  public function MyProfile(){
		
		$user = AuthService::getUserModel();
		
		$data = [
			"id" => $user->id, 
			"user_profile" => ($user->avatar) ? \AppConfig::USER_RESOURCE_URL . $user->avatar : '/img/user-thumb.png',
			"cover_pic" => ($user->cover_pic) ? \AppConfig::USER_RESOURCE_URL . $user->cover_pic : '/assets/images/avatars/profile-cover.jpg',
			"userdata" => $user,
		]; 
		 
		 
		 return view('profile' ,$data);	 
	  }	
	  
	  
	  		
		public function getChatNotification(){
			
           $user = AuthService::getUserModel();
		   
		   $chat_notification = DB::table('chat')->where('is_read', 0)->where('to_user_id', $user->id)->get();
		   
           $Array = [];
			
			foreach($chat_notification as $val){
				
			$fromUserData = DB::table('users')->where('id', $val->from_user_id )->first();
			
			$string = strip_tags($val->message);
			if (strlen($string) > 30) {

				// truncate string
				$stringCut = substr($string, 0, 30);
				$endPoint = strrpos($stringCut, ' ');

				$string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
				$string .= '...';
			}
				
			$Array['chat_notifications'][] = [
				    'id' => $val->id,
				    'from_user' => $fromUserData->fname.' '.$fromUserData->lname,
				    'image' => ($fromUserData->avatar) ? \AppConfig::USER_RESOURCE_URL . $fromUserData->avatar : '',
				    'message' => $string,
				    'is_read' => $val->is_read,
				    'to_user_id' => $val->to_user_id,
				    'from_user_id' => $val->from_user_id,
				    'created_at' => date("M d Y, H:i a", strtotime($val->created_at)),
				];
			}			
		 
		  return response()->json($Array);
		}

		public function countUnreadChatNotification(){
			
           $user = AuthService::getUserModel();
		   
		   $count = DB::table('chat')->where('is_read', 0)->where('to_user_id', $user->id)->get();
		   
            $CountArray = [];
		   
            $CountArray['count'] = count($count);		   

		  return response()->json($CountArray);	
		}	

		public function allChatReadNotification(){

		    $user = AuthService::getUserModel();
			
		    $arrUpdate = [ 'is_read' => 1 ];
		   
		   DB::table('chat')->where('to_user_id', $user->id)->update($arrUpdate);
		  return response()->json("READ_ALL_SUCCESS");	
		}
		
		public function updateTourStatus(Request $request){

		    $user = AuthService::getUserModel();
			
		    $arrUpdate = [ 'is_tour_read' => $request->get('tour_read') ];
		  
		   DB::table('users')->where('id', $user->id)->update($arrUpdate);
		  return response()->json("TOUR_READ_SUCCESS");	
		}
		
	public function EditProfilePic(Request $request)
    {
        $user = AuthService::getUserModel();
		
		$image = $request->file('photo');
		
		$directory_url = "/var/www/admin/public/avatar/";

		//$name = $user->id . '_' . time() . '_' . uniqid() . '.' . $image->getClientOriginalExtension();
		$name = $image->getClientOriginalName();
		
		
		$photo_resize = Image::make($image->getRealPath());   
	
		$photo_resize->resize(145, 145);
		
		
		$photo_resize->save($directory_url.$name);
		
		//$image->move($directory_url, $name);

        $picUpdate = [ 'avatar' => $name ];
		 
		
		DB::table('users')->where('id' , $user->id)->update($picUpdate);		
 
	   return response()->json("PIC_UPDATED");	
	}	
	
	public function EditCoverPic(Request $request)
    {
        $user = AuthService::getUserModel();
		
		$image = $request->file('Coverimgupload');

		$directory_url = "/var/www/admin/public/avatar/";
		$name = $image->getClientOriginalName();
		
		$photo_resize = Image::make($image->getRealPath());   
		$photo_resize->resize(1920, 1280);
		$photo_resize->save($directory_url.$name);

        $picUpdate = [ 'cover_pic' => $name ];
		 
		
		DB::table('users')->where('id' , $user->id)->update($picUpdate);		
 
	   return response()->json("COVER_PIC_UPDATED");	
	}	
	
	public function updateProfile(Request $request)
    {
        $user = AuthService::getUserModel();
		
        $data = $request->all();
		
		$userUpdate = [ 
		   'fname' => $data['fname'], 
		   'lname' => $data['lname'], 
		   'username' => $data['username'], 
		   'email' => $data['email'], 
		   'password' => Hash::make($data['password']), 
		   'forgot_password' => $data['password']
		  ];
		 
		
		DB::table('users')->where('id' ,$user->id)->update($userUpdate);		
 
	   return response()->json("PROFILE_UPDATED");	
	}
		

}
