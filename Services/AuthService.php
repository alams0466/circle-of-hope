<?php

namespace App\Services;
use Auth;
/**
 * 
 */

class AuthService extends BaseService {

    private static $userArray = null;
    private static $userModelObject = null;

    public static function setAuthUser($userObject) {
        self::$userArray = $userObject->toArray();
        self::$userModelObject = $userObject;
    }

    public static function getUserModel() {
        //return self::$userModelObject;
		return Auth::user();
    }

    public static function getUserArray() {
        return self::$userArray;
    }

}