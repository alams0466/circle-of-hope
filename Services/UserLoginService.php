<?php
namespace App\Services;

use App\Models\UserLoginModel;
use App\Repositories\UserLoginRepository;
use Jenssegers\Agent\Agent;

class UserLoginService extends BaseService{

    protected $repository = null;

    public function __construct()
    {
        $this->repository = new UserLoginRepository();
    }

    public function create($data)
    {
        $result = $this->repository->create($data);
        if (isset($result['error'])) {
            return ['error' => 'SERVER_ERROR'];
        }
        return $result;
    }

    public function update($id, $data)
    {
        $result = $this->repository->update($id, $data);
        if (isset($result['error'])) {
            return ['error' => 'SERVER_ERROR'];
        }
        return $result;
    }

    public function updateByCondition($condition, $data)
    {
        $result = $this->repository->updateByCondition($condition, $data);
        if (isset($result['error'])) {
            return ['error' => 'SERVER_ERROR'];
        }
        return $result;
    }

    public function deleteByCondition($condition)
    {
        $result = $this->repository->deleteByCondition($condition);
        if (isset($result['error'])) {
            return ['error' => 'SERVER_ERROR'];
        }
        return $result;
    }

    public function multipleDelete($ids)
    {
        $result = $this->repository->multipleDelete($ids);
        if (isset($result['error'])) {
            return ['error' => 'SERVER_ERROR'];
        }
        return $result;
    }

    public function find($id)
    {
        $login = $this->repository->find($id);
        if (is_null($login)) {
            return ['error' => 'USER_LOGIN_NOT_FOUND'];
        }
        return $login;
    }

    public function findByCondition($condition)
    {
        $login = $this->repository->findByCondition($condition);
        if (is_null($login)) {
            return ['error' => 'USER_LOGIN_NOT_FOUND'];
        }
        return $login;
    }

    public function getByCondition($condition)
    {
        return $this->repository->getByCondition($condition);
    }


    public function getUserLogins($userId)
    {
        return $this->getByCondition(['user_id' => $userId]);
    }
}