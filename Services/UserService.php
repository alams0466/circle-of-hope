<?php

namespace App\Services;


use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Services\UtilityService;
use DB;


/**
 *
 */
class UserService extends BaseService
{
	private $userRepository = null;


	public function __construct()
	{
		$this->userRepository = new UserRepository();

	}

	public function getUserForLogin($login_id, $email_id)
	{

		return $this->userRepository->getUserForLogin($login_id, $email_id);
	}

	public function loginUser($login_id, $email_id,$userPassword)
	{


		$user = $this->getUserForLogin($login_id, $email_id);
        
		if (is_null($user)) {
			
			return ['error' => 'ACCOUNT_NOT_FOUND'];
		}
		
       if($user->role_id != 4){
		   
		   return ['error' => 'ACCOUNT_NOT_ACTIVE'];
		   
	   }       
	   
	   if($user->status == 'inactive'){
		   
		   return ['error' => 'ACCOUNT_INACTIVE'];
		   
	   }	
	   
	   if($user->status == 'blocked'){
		   
		   return ['error' => 'ACCOUNT_BLOCKED'];
		   
	   }
	   
	   else if($user->email_verified == 0){
		   
		   return ['error' => 'EMAIL_NOT_VERIFIED'];
		   
	   }	
	 
      
	   //$password = Hash::make($userPassword);
	
		 
		if (!($user && Hash::check($userPassword, $user->password))) {
			  
			return ['error' => 'LOGIN_INVALID_CREDENTIALS'];
		}
		


		return ['user' => $user];
	}
	
	public function getUsers()
	{

		return $this->userRepository->getUsers();
	}		
	
	public function getUsersbyId($id)
	{
		return $this->userRepository->getUsersbyId($id);
	}

	public function userAdd($data)
	{
       	 				
	  $values = [
		   'fname' => $data['fname'], 
		   'lname' => !empty($data['lname']) ? $data['lname'] : null,		   
		   'mobile_number' => isset($data['mobile_number']) ? $data['mobile_number'] : null,			   
		   'username' => $data['username'],			   
		   'gender' => $data['gender'],			   
		   'age' => $data['age'],			   
		   'country_id' => $data['country_id'],			   
		   'email' =>  isset($data['email']) ? $data['email'] : '',			   
		   'role_id' => 4,			   
		   'password' => Hash::make($data['password']),			   
		   'forgot_password' => $data['password'],			   	   
		];
		
			  
		$user_id =  DB::table('users')->insertGetId($values);

		return $user_id;	 
 
	}
	
	    public function sendOtpforSignup($email)
	{
		
		$smsService = new SmsService();

		$getUserDetails = $this->checkAlreadyExist($email);
		
		if (is_null($getUserDetails)) {
			return ['error' => 'USER_NOT_FOUND'];
		}
         
		$send_otp = $smsService->sendSignupOTPMail($getUserDetails);

		$trackingID['tracking_id'] = UtilityService::getEncrytedValue($getUserDetails['id']);
		return $trackingID;
	}
	
	
	public function deleteUser($id)
	{
		return $this->userRepository->deleteUser($id);
	}

	
	public function checkAlreadyExist($emailID)
	{
		return $this->userRepository->checkAlreadyExist($emailID);
	}	
	
	public function checkAlreadyExistUsername($username)
	{
		return $this->userRepository->checkAlreadyExistUsername($username);
	}	
	
	
	public function insertOTP($otp, $userId){
		
		$this->userRepository->update($otp, $userId);
	}	
	
	public function email_verified($data, $userId){
		
		$this->userRepository->update($data, $userId);
	}



	
    public function sendOtp($email)
	{
		
		$smsService = new SmsService();

		$getUserDetails = $this->checkAlreadyExist($email);
		
		if (is_null($getUserDetails)) {
			return ['error' => 'USER_NOT_FOUND'];
		}
         
		$send_otp = $smsService->sendOTPMail($getUserDetails);

		$trackingID['tracking_id'] = UtilityService::getEncrytedValue($getUserDetails['id']);
		return $trackingID;
	}

    public function resendOtp($data)
	{
		
		$smsService = new SmsService();
		$email = $data['email'];

		$getUserDetails = $this->checkAlreadyExist($email);
		
		if (is_null($getUserDetails)) {
			return ['error' => 'USER_NOT_FOUND'];
		}
         
		$send_otp = $smsService->sendOTPMail($getUserDetails);

		$trackingID['tracking_id'] = UtilityService::getEncrytedValue($getUserDetails['id']);
		return $trackingID;
	}
	
	
	public function verifyOTP($data)
	{
       
		$user = $this->checkAlreadyExist($data['email']);

		if (is_null($user)) {
			return ['error' => 'USER_NOT_FOUND'];
		}
         
		if ($user['mobile_otp'] != $data['otp_code']) {
			return ['error' => 'INVALID_OTP'];
		}


		return ['user' => $user];
	}
	
	public function resetPassword($data)
	{
		
		$userId = UtilityService::getDecryptedValue($data['trackID']);
		$newPassword = UtilityService::generatePasswordForDb($data['new_password']);

		$data = [
			'password' => $newPassword,
			'forgot_password' => $data['new_password']
		];
		
		
		$updateResult = $this->userRepository->update($data, $userId);
		if (isset($updateResult['error'])) {
			return ['error' => $updateResult['error']];
		}

		return true;
	}
	

}
