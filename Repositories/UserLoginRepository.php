<?php

namespace App\Repositories;

use App\Models\UserLoginModel;
use App\Models\BaseModel;


class UserLoginRepository extends BaseRepository{

    public function create($data){

        try{
            return UserLoginModel::create($data);
        } catch (\Exception $e){
            return ['error' => $e->getMessage()];
        }
    }

    public function update($id, $data){
        try{
            return UserLoginModel::find($id)->update($data);
        } catch (\Exception $e){
            return ['error' => $e->getMessage()];
        }
    }

    public function updateByCondition($condition, $data)
    {
        try {
            return UserLoginModel::where($condition)->update($data);
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function deleteByCondition($condition)
    {
		try {
            return UserLoginModel::where($condition)->delete();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function multipleDelete($ids)
    {
        try {
            return UserLoginModel::whereIn('id', $ids)->delete();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function find($id)
    {
    	return UserLoginModel::find($id);
    }

    public function findByCondition($condition)
    {
    	return UserLoginModel::where($condition)->first();
    }

    public function getByCondition($condition)
    {
        return UserLoginModel::where($condition)->get();
    }
}