<?php
namespace App\Repositories;

use App\Models\User;
use DB;

/**
* 
*/
class UserRepository extends BaseRepository
{
	public function create($data)
	{
		
		$user = User::insert($data);

		return $user;
	}

	public function find($id)
	{
		return User::find($id);
	}


	public function update($data, $id)
	{
		
			return User::where('id', $id)->update($data);
	}

	public function getUserForLogin($login_id, $email_id)
	{	
		
		$user='';
		if(empty($email_id) && !empty($login_id)){	
		   $user = User::where('username', $login_id);
		}		
		
		if(!empty($email_id) && empty($login_id)){	
		   $user = User::where('email', $email_id);
		}
		
		return $user->first();
	}	
	
	public function getUsers()
	{	
		return User::with('role', 'latestLogin')->where('role_id', 4)->orderBy('id', 'DESC')->get();
				
	}
		
	
	public function getUsersbyId($id)
	{	
		return User::where('id', $id)->first();
				
	}	
	
	
	public function searchUser($status,$from_date,$to_date)
	{			
	
		if(!empty($status) && empty($from_date) && empty($to_date)){
			
			$result =   User::with('role', 'latestLogin')->where('role_id', 4)->where('status', $status);

		}		
		
		if(!empty($from_date) && !empty($to_date) && empty($status)){
			
		$result =  User::with('role', 'latestLogin')
			->where('role_id', 4)
			->whereDate('created_at', '>=', $from_date)
			->whereDate('created_at', '<=', $to_date);
		}
		
		if(!empty($status) && !empty($from_date) && !empty($to_date)){
			
		 $result =  User::with('role', 'latestLogin')
			->where('role_id', 4)
			->where('status', $status)
			->whereDate('created_at', '>=', $from_date)
			->whereDate('created_at', '<=', $to_date);
		}
		
         if(empty($status) && empty($from_date) && empty($to_date)){
			
			$result = User::with('role', 'latestLogin')->where('role_id', 4);
		 }

		return $result->orderBy('id', 'DESC')->get();
				
	}

	
		
	
	public function deleteUser($id)
	{
		 $result = DB::table('users')->delete($id);

		return $result;
	}
		
	public function checkAlreadyExist($emailID)
	{	
	
		return User::where('email', $emailID)->first();
				
	}
	
	public function checkAlreadyExistUsername($username)
	{	
		return User::where('username', $username)->get();
				
	}

	public function submitTagMessage($values)
	{
     
		return DB::table('tags_responder')->insertGetId($values);
		
	}
	
	public function updateTag($id,$value)
	{
	
		return DB::table('tags')->where('id', $id)->update($value);
	}
	
	
	public function AddTag($values)
	{
     
		return DB::table('tags')->insertGetId($values);
		
	}
	
	public function updateTagMessage($id,$value)
	{
		
		return DB::table('tags_responder')->where('id', $id)->update($value);
	}	
		
	
 	
}
?>