<?php
Route::group(['middleware' => 'NotAuthenticate'], function ($routes) {
    
	Route::get('/login', function () {
        return view('login');
    });
   
	
    Route::get('/forgot-password', function () {
        return view('forgot_password');
    });
	
});
Route::group(['middleware' => 'web'], function ($routes) {
	   
	
	$routes->get('/', function () { return view('homepage'); }); 
	$routes->post('/login', 'UserController@login');
	$routes->get('/about-us', function () { return view('about_us'); }); 
	$routes->get('/why-circle-of-hope', function () { return view('why_circle_of_hope'); }); 
	$routes->get('/how-its-works', function () { return view('how_its_works'); }); 
	$routes->get('/contact-us', function () { return view('contact_us'); }); 
	$routes->get('/resources', function () { return view('resources'); }); 	
	$routes->get('/terms-of-service', function () { return view('terms-conditions'); }); 
	$routes->get('/privacy-policy', function () { return view('privacy-policy'); }); 
    $routes->get('/resource/help-line', 'HomeController@getHelpLine');	
    $routes->get('/register', 'UserController@signup');
    
    //$routes->post('/forgot-password', 'UserController@forgotPassword');
    $routes->post('/email-check', 'UserController@checkAlreadyExist');
    $routes->post('/check-email', 'UserController@checkEmailyExist');
    $routes->get('/get-otp/{tracking_id}', 'UserController@ShowOtpForm');
    $routes->get('/get-otp-verification/{trackingID}', 'UserController@getOTPVerificationForm');
    $routes->post('/verify-otp', 'UserController@verifyOTP');
    $routes->post('/verify-otp-signup', 'UserController@verifyOTPSignup');
    $routes->get('/set-new-password/{track_id}', 'UserController@newPasswordForm');
    $routes->post('/reset-password', 'UserController@resetPassword');
    $routes->post('/resend-otp', 'UserController@resendOtp');
    $routes->post('/submit-email', 'UserController@registerEmail');
    $routes->get('/signup-step-two/{trackID}', 'UserController@signupStepTwo');
    $routes->post('/register-submit', 'UserController@userAdd');
    $routes->post('/username-check', 'UserController@checkAlreadyExistUsername');
});
Route::group(['middleware' => 'auth'], function ($UserRoutes) {	
    $UserRoutes->get('/home', 'HomeController@home');
	$UserRoutes->get('/all-tags', 'HomeController@getAllTags');
    $UserRoutes->get('/logout', 'UserController@logout');
    $UserRoutes->post('/post-store', 'PostController@store');
    $UserRoutes->post('/post-update', 'PostController@postUpdate');
    $UserRoutes->post('/post', 'PostController@index');
    $UserRoutes->get('/post/{postID}', 'PostController@getPost');
    $UserRoutes->post('/post/comment/store', 'PostController@commentStore');
    $UserRoutes->post('/post/comment', 'PostController@comments');
    $UserRoutes->post('/post/comment/reply/store', 'PostController@replyStore');
    $UserRoutes->post('/post/comment/{commentId}/reply', 'PostController@getCommentReply');
    $UserRoutes->post('/report-submit', 'PostController@reportPostSubmit');
    $UserRoutes->post('/comment-report-submit', 'PostController@CommentreportPostSubmit');
    $UserRoutes->post('/reply-report-submit', 'PostController@ReplyReportSubmit');
    $UserRoutes->get('/notification', 'UserController@getNotification');
	$UserRoutes->post('/read-notification', 'UserController@readNotification');
	$UserRoutes->get('/count-unread-notification', 'UserController@countUnreadNotification');
	$UserRoutes->get('/delete-notification', 'UserController@deleteNotification');
	$UserRoutes->get('/allRead-notification', 'UserController@allReadNotification');
	$UserRoutes->get('/allChat-Read-notification', 'UserController@allChatReadNotification');
	$UserRoutes->get('/chat-notification', 'UserController@getChatNotification');
	$UserRoutes->get('/count-unread-chat-notification', 'UserController@countUnreadChatNotification');
	$UserRoutes->post('/delete-post', 'PostController@Postdelete');
	$UserRoutes->post('/delete-comment', 'PostController@CommentDelete');
	$UserRoutes->post('/delete-reply', 'PostController@ReplyDelete');
	$UserRoutes->post('/edit-post', 'PostController@EditPost');
	$UserRoutes->post('/delete-post-image', 'PostController@deletePostImage');
	$UserRoutes->post('/likes', 'PostController@postLikes');
	$UserRoutes->post('/comment-likes', 'PostController@commentLikes');
	$UserRoutes->post('/reply-likes', 'PostController@replyLikes');
	$UserRoutes->get('/my-profile', 'UserController@MyProfile');
	//chat
	$UserRoutes->get('/private-message/{userId}/{post_id}', 'PostController@messagePrivate');
	$UserRoutes->post('/chat-submit', 'PostController@chatSubmit');
	
	$UserRoutes->post('/post/chat-messages', 'PostController@getChatMessages');
	$UserRoutes->post('/tour-read', 'UserController@updateTourStatus');
	$UserRoutes->post('/uploadProfilePic', 'UserController@EditProfilePic');
	$UserRoutes->post('/uploadCoverPic', 'UserController@EditCoverPic');
	$UserRoutes->post('/update-user', 'UserController@updateProfile');
	$UserRoutes->get('/groups', 'GroupController@getGroup');
	$UserRoutes->get('/create-group', 'GroupController@groupForm');
	$UserRoutes->post('/createGroup', 'GroupController@CreateGroup');
	
	//Stories section
	$UserRoutes->get('/stories', 'StoryController@getStory');
	
});
